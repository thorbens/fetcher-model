/**
 * Interface for a basic response.
 */
export interface Response {
    /**
     * The response body.
     */
    readonly body?: string;
    /**
     * The status code for this response.
     */
    readonly status: number;
    /**
     * The content type for this response.
     */
    readonly contentType?: string;

    /**
     * Retrieves the response body as JSON Object.
     *
     * @typeparam The type of the object after parsing.
     */
    asJson<T>(): T;
}
