import {FetchOptions} from "./FetchOptions";
import {Response} from "./Response";

/**
 * A fetcher provides functionality to fetch an url with given options and returns a Response.
 */
export interface Fetcher {
    /**
     * Fetches the given url with the given options and return a response.
     * If no method is defined in the options, a GET request should be performed.
     * If the response status is greater than 400, the promise should be rejected.
     *
     * @param url The url to fetch.
     * @param options Options to use for fetching the given url.
     */
    fetch(url: string, options?: FetchOptions): Promise<Response>;
}
