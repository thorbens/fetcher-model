/**
 * Enum for Http Methods.
 */
export enum Method {
    GET = "get",
    PUT = "put",
    DELETE = "delete",
    POST = "post",
}

/**
 * Interface for fetch options.
 */
export interface FetchOptions {
    /**
     * The method to use.
     */
    method?: Method;
    /**
     * Headers that will be set for the request.
     */
    headers?: { [name: string]: string };
    /**
     * Data to send with the request.
     */
    data?: object;
    /**
     * Form data to send with the request.
     */
    formData?: { [key: string]: string | number | object };
}
